const fs = require('fs');
const carbone = require('carbone');

const cow = {
	number: 'FR6123382498',
	name: 'GJM JUDO',
	isu: {
		value: 120,
		determinationCoef: null,
	},
	lait: {
		value: 3,
		determinationCoef: null,
	},
	tp: {
		value: 0.1,
		determinationCoef: null,
	},
	tb: {
		value: 2.4,
		determinationCoef: null,
	},
	mp: {
		value: 2,
		determinationCoef: null,
	},
	mg: {
		value: 28,
		determinationCoef: null,
	},
	inel: {
		value: 0.5,
		determinationCoef: 58,
	},
	mo: {
		value: 0.2,
		determinationCoef: 46,
	},
	ma: {
		value: 0.2,
		determinationCoef: null,
	},
	cc: {
		value: -0.2,
		determinationCoef: null,
	},
	me: {
		value: 0.2,
		determinationCoef: null,
	},
	ps: {
		value: 0.2,
		determinationCoef: null,
	},
	pj: {
		value: 0,
		determinationCoef: null,
	},
	eq: {
		value: -0.1,
		determinationCoef: null,
	},
	aa: {
		value: 0.3,
		determinationCoef: null,
	},
	ah: {
		value: 0.8,
		determinationCoef: null,
	},
	ea: {
		value: -0.1,
		determinationCoef: null,
	},
	ia: {
		value: 0.3,
		determinationCoef: null,
	},
	lt: {
		value: 0,
		determinationCoef: null,
	},
	hs: {
		value: -0.4,
		determinationCoef: null,
	},
	lp: {
		value: -0.2,
		determinationCoef: null,
	},
	pc: {
		value: -0.3,
		determinationCoef: null,
	},
	as: {
		value: 0.1,
		determinationCoef: null,
	},
	ec: {
		value: 0,
		determinationCoef: null,
	},
	is: {
		value: 0.1,
		determinationCoef: null,
	},
	ib: {
		value: 0.8,
		determinationCoef: null,
	},
	aj: {
		value: 0.7,
		determinationCoef: null,
	},
	pi: {
		value: 0.2,
		determinationCoef: null,
	},
	mr: {
		value: -0.5,
		determinationCoef: null,
	},
	lo: {
		value: 0.3,
		determinationCoef: null,
	},
	stma: {
		value: 1.1,
		determinationCoef: null,
	},
	repro: {
		value: 0.4,
		determinationCoef: null,
	},
	lgf: {
		value: -0.2,
		determinationCoef: null,
	},
	tr: {
		value: -1.4,
		determinationCoef: null,
	},
	te: {
		value: -0.7,
		determinationCoef: null,
	},
	cell: {
		value: 1.3,
		determinationCoef: 49,
	},
	macl: null,
	ferv: null,
	ferg: null,
	ivia1: null,
	fnai: null,
	fvel: null,
	vnai: null,
	vvel: null,
};
/**
 *
 * @param {object} data
 * @param {object} options
 */
function print(data, options) {
	carbone.render('./templates/chart.ods', data, options, (err, result) => {
		if (err) {
			return console.log(err);
		}
		// write the result
		fs.writeFileSync(`./docs/result.${options.convertTo}`, result);
		console.log('process terminé');
		return process.exit();
	});
}

const options = {
	convertTo: 'pdf',
};

print(cow, options);
